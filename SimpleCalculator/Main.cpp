//Simple Calculator
//Kathy Xiong

#include <iostream>
#include <conio.h>

using namespace std;

float Add(float num1, float num2);

float Subtract(float num1, float num2);

float Multiply(float num1, float num2);

bool divide(float num1, float num2, float& ans);

int main()
{
	float num1 = 0;
	float num2 = 0;
	char op;


	char input = 'y';
	while (input == 'y')
	{
		cout << "Enter two positive numbers: ";
		cin >> num1;
		cin >> num2;

		if (num1 >= 0 && num2 >= 0) //To make sure the input numbers are both positive
		{
			cout << "Enter the a character for the operator such as +, -, *, or / : " << "\n";
			cin >> op;

			if (op == '+')
			{
				cout << Add(num1, num2) << "\n";
			}
			if (op == '-')
			{
				cout << Subtract(num1, num2) << "\n";
			}
			if (op == '*')
			{
				cout << Multiply(num1, num2) << "\n";
			}
			if (op == '/')
			{
				float answer = 0;
				if (divide(num1, num2, answer))
				{
					cout << answer << "\n";
				}
				else
				{
					cout << "You cannot divide by zero.\n";
				}
			}
		}
		else {
			cout << "Please enter positive numbers. ";
		}
		cout << "again? ";
		cin >> input;
	}

	return 0;
}

float Add(float num1, float num2)
{
	float answer = num1 + num2;
	return answer;
}

float Subtract(float num1, float num2)
{
	float answer = num1 - num2;
	return answer;
}

float Multiply(float num1, float num2)
{
	float answer = num1 * num2;
	return answer;
}

bool divide(float num1, float num2, float& ans)
{
	if (num2 == 0) return false;

	ans = num1 / num2;

	return true;
}